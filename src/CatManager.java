import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	private ArrayList<Cat>  myCats = new ArrayList<Cat>();

	public CatManager()
	{
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
	}


	public void add(Cat aCat)
	{
		myCats.add(aCat);
	}


	public Cat findThisCat(String name)
	{
		int foundCat = 0;
		for (int i=0; i < myCats.size(); i++)
		 {
			if (myCats.get(i).getName() == name) {
				return myCats.get(i);
			}
			foundCat = i;
		 }
		return myCats.get(foundCat);
	}

	
	

	public int countColors(String color)
	{           
		int colorNum = 0;
			for (int i=0; i < myCats.size(); i++)
			{
				
			if (myCats.get(i).getColor() == color) {
				colorNum++;
			     }
			}
			return colorNum;
	
	}
	
}
